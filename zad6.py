import paramiko

hostname = input("nazwa hosta")
login = input("login")
sciezka_lokalna = input("sciezka lokalna")
sciezka_zdalna = input("sciezka zdalna")

ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())


try:
    ssh_client.connect(hostname, username=login)
except Exception as e:
    print(f"Blad podczas laczenia z serwerem: {e}")

try:
    sftp = ssh_client.open_sftp()
    sftp.put(sciezka_lokalna, sciezka_zdalna)
    sftp.close()
except Exception as e:
    print(f"blad: {e}")
